let arr = ["one","two","three","four",["one", "two", "three",["one","two"]]];
let container=document.getElementById("container");


function writeChilds(arr,par=container){
    let parent=document.createElement("ul");
    par.appendChild(parent);
    arr.map(element => {

        if(Array.isArray(element)){

            writeChilds(element,parent)
        }
        else
        {
            let li= document.createElement("li");
            li.innerHTML=element;
            parent.appendChild(li);
        }
    });

}


function clear(t){
    let time = document.createElement('p');
    time.innerText = t;
    time.style.color="blue"
    time.style.fontSize="40px"
    document.body.appendChild(time);
    setInterval(() => --time.innerText, 1000);
    setTimeout(() => document.body.innerHTML = "", 10000);
}


clear(10);
writeChilds(arr,container);