let arr = [11, "ab", "22", 22, "fart", {ac: 12}, { name: 'Hasan'}, null];
console.log("Before procedure:",arr)
let type = prompt("Type to delete", "number");
console.log(filterBy(arr, type));


function filterBy(arr, type) {
    if (type === 'null') {
        return arr.filter(x => x !== null);
    } else if (type === 'object') {
        return arr.filter(x => typeof x !== 'object' || x === null);
    } else {
        return arr.filter(x => typeof x !== type)
    }
}
