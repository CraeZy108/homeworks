$('a[href*="#"]').on('click', function(e) {
    e.preventDefault();

    $('html, body').animate(
        {
            scrollTop: $($(this).attr('href')).offset().top,
        },
        600,
        'linear'
    )
});

$('#slideBtn').on('click',function(){
    $('#section-popular').slideToggle()
});



$(window).scroll(function() {
    if ($(this).scrollTop() >= $(window).height()) {
        $('#up').fadeIn();
    } else {
        $('#up').fadeOut();
    }
});

