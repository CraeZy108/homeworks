let tabsContent = document.querySelector("#tabContents");
let tabs = document.querySelector("#tabs");

for (let i = 0; i < tabs.length; i++) {
    tabs.children[i].dataset.index = i;
    tabsContent.children[i].dataset.index = i;
}

for (let i = 0; i < tabContents.children.length; i++){
    tabs.children[i].dataset.index = i;
    if(i)
        tabContents.children[i].hidden = true;
}

tabs.onclick = e => {
    tabs.querySelector(".active").classList.remove("active");
    tabContents.querySelector("li:not([hidden])").hidden = true;
    e.target.classList.add("active");
    tabsContent.children[e.target.dataset.index].hidden = false;
};
