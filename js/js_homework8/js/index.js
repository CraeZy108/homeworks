let textInput=document.getElementsByClassName("form-text")[0];

let span=document.createElement("span");
span.classList.add("span");

let buttonX=document.createElement("a");
buttonX.innerHTML="X";
buttonX.classList.add("btn");
buttonX.classList.add("btn-rounded");
buttonX.addEventListener("click",(e)=>{
    e.preventDefault();
    e.target.parentElement.remove();
    textInput.value="";
});

let error=document.createElement("span");
error.innerHTML="Please enter a valid price";
error.className="error-text";
error.name="error";

textInput.onfocus = (event)=>event.target.style.border="2px solid lime";

textInput.onblur = (event)=>{
    if(!(+event.target.value < 0))
        event.target.style.borderColor="";
};

textInput.onchange = (event)=>{
    span.remove();

    if(+event.target.value<0){

        textInput.parentElement.appendChild(error);
        textInput.style.border = "2px solid red";
    }
    else{
        error.remove();

        span.innerHTML=`Current Price: ${textInput.value}`;
        span.appendChild(buttonX);
        document.getElementById("tag-container").appendChild(span);

    }
};

