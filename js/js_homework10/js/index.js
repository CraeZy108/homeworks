let icons = document.getElementsByClassName("icon-password");
let btn=document.getElementsByClassName("btn")[0];
let span=document.createElement("span");
let inputs= document.querySelectorAll("input");

span.innerHTML="Passwords do not match";
span.style.color="red";
span.hidden=true;
inputs[1].parentNode.parentNode.appendChild(span);


for (const icon of icons) {
    icon.addEventListener("click", (e) => {
        if (icon.previousElementSibling.type === "password") {
            icon.previousElementSibling.type = "text";
            icon.classList.remove("fa-eye");
            icon.classList.add("fa-eye-slash");
        } else {
            icon.previousElementSibling.type = "password";
            icon.classList.add("fa-eye");
            icon.classList.remove("fa-eye-slash");
        }
    })
}

btn.addEventListener("click",(e)=>{
    e.preventDefault();
    if(inputs[0].value !== inputs[1].value){
        span.hidden=false;
    }
    else{
        span.hidden=true;
        alert("Welcome!");
    }

});
