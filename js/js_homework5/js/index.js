let firstName = prompt("What is your name?");
let lastName = prompt("What is your surname?");


var user = createNewUser(firstName,lastName);

console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());

function createNewUser(firstName="",lastName = ""){
    let birthDay = prompt("What is your birth date? (dd.mm.yyyy)", "");


    let user={
        set birthDay(birthDay){
            this._birthDay = birthDay;
        },
        set firstName(name){
            this._firstName = firstName;
        },
        set lastName(surname){
            this._lastName = lastName;
        },
        "getLogin":function() {
            return this._firstName.charAt(0).toLowerCase() + this._lastName.toLowerCase();
        },
        "getAge": function(){
            let now = new Date();
            let birthArr = this._birthDay.split(".");

            let birthDate = new Date(birthArr[2],birthArr[1]-1,birthArr[0]);
            let age = now.getFullYear() - birthDate.getFullYear();
            let m = now.getMonth() - birthDate.getMonth();
            if (m < 0 || (m === 0 && now.getDate() < birthDate.getDate())) {
                age--;
            }
            return age;
        },
        "getPassword": function(){
            return this._firstName[0].toUpperCase() + this._lastName.toLowerCase() + this._birthDay.slice(-4);
        }

    }
    user.firstName = firstName;
    user.lastName = lastName;
    user.birthDay = birthDay;

    return user;

}